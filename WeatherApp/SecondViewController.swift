//
//  SecondViewController.swift
//  WeatherApp
//
//  Created by Nurba on 20.04.2021.
//

import UIKit
import Alamofire

class SecondViewController: UIViewController {

    
    var decoder: JSONDecoder = JSONDecoder()
    var jsonData: Welcome11?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SecondCell.nib, forCellReuseIdentifier: SecondCell.identifier)
        tableView.layer.cornerRadius = 13
        tableView.layer.masksToBounds = true
        getdata()
        
    }
    
    
    func getdata(){
        
        
        let request: String = "http://93.185.75.19:4003/simple/averages/last?key=3a439437bf331d0e2612d1f10d7484eef51f681520aa311024c280af74ec1d20"
        
        
        
        AF.request(request).responseJSON { (response) in
            switch response.result {
            case .success(_):
                guard let data = response.data else { return }
                guard let answer = try? self.decoder.decode(Welcome11.self, from: data) else{ return }
                print(answer)
                
                self.jsonData = answer
                self.tableView.reloadData()
                
                
                
                
            case .failure(let err):
                print(err.errorDescription ?? "")
            }
            
        }
    }
}
extension SecondViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: SecondCell.identifier, for: indexPath)
            as! SecondCell
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        
        let item = jsonData?[indexPath.item]
        
        let term = String(format: "%.1f", (item?.value ?? 0.1) * 1000)
        cell.codeLabel.text = item?.code
        
        cell.valueLabel.text = term
        cell.layer.cornerRadius = 13
        cell.layer.masksToBounds = true
        
        if (item?.value ?? 0.1) * 1000 < 40 && (item?.value ?? 0.1) * 1000 > -1{
            cell.myview.backgroundColor = UIColor(red: 0.82, green: 0.88, blue: 0.56, alpha: 1.00) //green
        } else if (item?.value ?? 0.1) * 1000 > 40 && (item?.value ?? 0.1) * 1000 < 100  {
            cell.myview.backgroundColor = UIColor(red: 0.99, green: 0.82, blue: 0.06, alpha: 1.00) // yellow
        }
        else if (item?.value ?? 0.1) * 1000 > 100 && (item?.value ?? 0.1) * 1000 < 200  {
            cell.myview.backgroundColor = UIColor(red: 1.00, green: 0.56, blue: 0.18, alpha: 1.00) // orange
            
        }else if (item?.value ?? 0.1) * 1000 > 200{
            cell.myview.backgroundColor = UIColor(red: 0.73, green: 0.40, blue: 0.40, alpha: 1.00) //red
            cell.codeLabel.tintColor = .white
            cell.unitLabel.tintColor = .white
            cell.valueLabel.tintColor = .white
        }
        return cell
    }
    
    
}
