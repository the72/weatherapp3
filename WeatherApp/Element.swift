// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - WelcomeElement
struct WelcomeElement: Decodable {
    let id: String
    let value: Double?
    let date: String
    let stationID: Int
    let code: String
    let unit: String

    enum CodingKeys: String, CodingKey {
        case id, value, date
        case stationID = "stationId"
        case code, unit
    }
}

typealias Welcome11 = [WelcomeElement]
