//
//  SecondCell.swift
//  WeatherApp
//
//  Created by Nurba on 20.04.2021.
//

import UIKit

class SecondCell: UITableViewCell {
  
    
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var myview: UIView!
    @IBOutlet weak var codeLabel: UILabel!
    static let identifier = String(describing: SecondCell.self)
    static let nib = UINib(nibName: identifier, bundle: nil)
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        myview.layer.cornerRadius = 8
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
