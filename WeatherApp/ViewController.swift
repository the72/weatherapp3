//
//  ViewController.swift
//  WeatherApp
//
//  Created by Nurba on 19.04.2021.
// 

import UIKit
import Alamofire

class ViewController: UIViewController {

    

    
    let Surl = "https://api.openweathermap.org/data/2.5/weather?lat=51.1801&lon=71.44598&appid=2654d3fe9bc4479d7f9be231c5ebcc6a&units=metric&lang=ru"
    
    var myData: Welcome!
   
    

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.cornerRadius = 12
        tableView.layer.masksToBounds = true
        tableView.register(WidgetCell.nib, forCellReuseIdentifier: WidgetCell.identifier)
        getData()

   }
 


    

    
    func getData(){
        
        
        AF.request(Surl).responseJSON { (response) in
            switch response.result{
            case .success(_):
                guard let data = response.data else { return }
                guard let answer = try? JSONDecoder().decode(Welcome.self, from: data) else{ return
                }
                self.myData = answer
           
                print(self.myData)
                
                self.tableView.reloadData()
            case .failure(let err):
                print(err.errorDescription ?? "errror")
            }
        }
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: WidgetCell.identifier, for: indexPath) as! WidgetCell
        let term = String(format: "%.0f", (myData?.main.temp ?? 1))
        
        cell.tempLabel.text = term + "º"
        cell.desc.text = myData?.weather.first?.weatherDescription ?? "nil"
        cell.windSpeed.text = "\(myData?.wind.speed ?? 0) м/с"
        cell.feelsLike.text = " \(myData?.main.feelsLike ?? 0.0)°"
        cell.pressure.text = "\(myData?.main.pressure ?? 0)"
        
       
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "second")
      

        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
}
